namespace Task2_Rectangle
{
    public struct Point
    {
        public int X { get; set; }

        public int Y { get; set; }

        public Point(int x, int y) : this()
        {
            X = x;
            Y = y;
        }

        public Point Move(int moveX, int moveY)
        {
            this.X += moveX;
            this.Y += moveY;

            return new Point(X, Y);
        }
    }
}