using System;
using System.Linq;
using System.Text;

namespace Task1_Vector
{
    public class Vector
    {
        private int[] _vector;

        private int _size;

        public Vector(int size)
        {
            _vector = new int[size];
            _size = size;
        }

        public int GetElementById(int id)
        {
            if (!OutOfBounds(id))
                return _vector[id];
            else
                return _vector[_size];
        }

        public void SetElementById(int id, int value)
        {
            _vector[id] = value;
        }

        public Vector SetInterval(int start, int end)
        {
            var size = end - start;
            var result = new Vector(size);

            if (!OutOfBounds(end))
                Array.Copy(_vector, start, result._vector, 0, size);
            return result;
        }

        public bool SameSize(Vector v)
        {
            return _size == v._size;
        }

        public static Vector Add(Vector a, Vector b)
        {
            var result =  new Vector(a._size);

            if (a.SameSize(b))
                for (var i = 0; i < a._size; i++)
                    result._vector[i] = a._vector[i] + b._vector[i];

            return result;
        }

        public static Vector Substract(Vector a, Vector b)
        {
            var result = new Vector(a._size);

            if(a.SameSize(b))
                for (var i = 0; i < a._size; i++)
                    result._vector[i] = a._vector[i] - b._vector[i];

            return result;
        }

        public Vector Multiply(int scalar)
        {
            var result = new Vector(_size);

            for (var i = 0; i < _size; i++)
                result._vector[i] = _vector[i] * scalar;

            return result;
        }

        public bool Equals(Vector a)
        {
            if (!SameSize(a)) return false;
            for (var i = 0; i < _size; i++)
                if(_vector[i] != a._vector[i])
                    return false;
            return true;
        }

        public bool OutOfBounds(int index)
        {
            if (index > _size)
                throw new ArgumentOutOfRangeException();
            return false;
        }

        public void Populate(Random r)
        {
            for (var i = 0; i < _vector.Length; i++)
                _vector[i] = r.Next(-10, 10);
        }

        public override string ToString()
        {
            var vector = new StringBuilder();

            vector.Append("[");
            foreach (var item in _vector)
                vector.Append($" {item} ");
            vector.Append("]");

            return vector.ToString();
        }
    }
}