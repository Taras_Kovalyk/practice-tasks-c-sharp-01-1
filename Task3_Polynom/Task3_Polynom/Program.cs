﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using static System.Math;

namespace Task3_Polynom
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] indexes = {1, 2, 3};
            int[] otherIndexes = {-1, -2, -3};

            Polynom first = new Polynom(indexes);
            Polynom second = new Polynom(otherIndexes);

            var sum = second.Add(first);
            var substraction = second.Substract(first);
            var firstCalculated = first.Calculate(3);
            var secondCalculated = second.Calculate(10);

            Console.WriteLine($"first polynom {first}\n");
            Console.WriteLine($"second polynom {second}\n");

            Console.WriteLine($"first + second = {sum}\n");
            Console.WriteLine($"first - second = {substraction}\n");

            Console.WriteLine($"first polynom calculated with X=3  f(3) = {firstCalculated}");
            Console.WriteLine($"second polynom calculated with X=0  f(10) = {secondCalculated}");

            Console.ReadLine();
        }
    }
}
