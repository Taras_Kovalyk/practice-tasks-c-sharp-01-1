﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task4_Matrix
{
    class Program
    {
        static void Main(string[] args)
        {
            Random r = new Random();

            Matrix m1 = new Matrix(3, 3);
            m1.Populate(r);

            Matrix m2 = new Matrix(3, 3);
            m2.Populate(r);

            //m1 matrix minor with removing 0 row and 0 column
            var z = Matrix.Minor(m1, 0, 0);

            //Minor of m2 matrix wont calculate since it is not square
            var z2 = Matrix.Minor(m2, 0, 0);

            var multiplication = Matrix.Multiply(m1, m2);
            var addition = Matrix.Add(m1, m2);
            var substraction = Matrix.Substract(m1, m2);

            Console.WriteLine($"first matrix =\n{m1.ToString()}");
            Console.WriteLine($"second matrix =\n{m2.ToString()}");

            Console.WriteLine($"m1 * m2 =\n{multiplication}");
            Console.WriteLine($"m1 + m2 =\n{addition}");
            Console.WriteLine($"m1 - m2 =\n{substraction}");
            Console.WriteLine($"Matrix minor\n{z.ToString()}");

            Console.Read();
        }
    }
}
